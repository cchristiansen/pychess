import sys
from backend import *

def getridofPunctuation(str):
	take = 0
	str = str.translate(None, "!?+")
	if "x" in str[1:4]:
		take = 1
		str = str.translate(None, 'x')
	return str,take

def decodeHuman(player,str,take):
	if (len(str) == 2) and (str[0] in "abcdefgh") and (str[1] in "12345678"):
		mtype = "normalpawn"
	elif (len(str) == 3) and (str[0] in "abcdefgh") and (str[1] in "12345678") and (str[2] in "QRBN"):
		mtype = "normalpromotepawn"
	elif (len(str) == 3) and (str[0] in "KQRBNp") and (str[1] in "abcdefgh") and (str[2] in "12345678"):
		mtype = "normalpiece"
	elif str == "0-0":
		mtype = "castlingkingside"
	elif str == "0-0-0":
		mtype = "castlingqueenside"
	elif (len(str) == 4) and (str[0] in "abcdefgh") and (str[1] in "12345678") and (str[2] in "abcdefgh") and (str[3] in "12345678"):
		mtype = "coord"
	elif (len(str) == 5) and (str[0] in "KQRBNp") and (str[1] in "abcdefgh") and (str[2] in "12345678") and (str[3] in "abcdefgh") and (str[4] in "12345678"):
		mtype = "piececoord"
	elif (len(str) == 3) and (str[0] in "abcdefgh") and (str[1] in "abcdefgh") and (str[2] in "12345678"):
		mtype = "specialxpawn"
	elif (len(str) == 4) and (str[0] in "RBNp") and (str[1] in "abcdefgh") and (str[2] in "abcdefgh") and (str[3] in "12345678"):
		mtype = "specialxpiece"
	elif (len(str) == 4) and (str[0] in "RBNp") and (str[1] in "12345678") and (str[2] in "abcdefgh") and (str[3] in "12345678"):
		mtype = "specialypiece"
	elif (len(str) == 2) and (str[0] in "KQRBNp") and (str[1] in "KQRBNp") and take == 1:
		mtype = "doublespecial"
	else:
		return "Unknown Notation"
	return mtype

def run(player,str):
	promoteTo = 0
	str,take = getridofPunctuation(str)
	mtype = decodeHuman(player,str,take)
	if mtype == "Unknown Notation":
		return mtype
	if mtype[-4:] == "pawn":
		pieceType = player+"p"
	elif mtype[-5:] == "piece":
		pieceType = player+str[0]
	elif mtype[-5:] == "coord":
		posTo = chessToCoord(str[-2:])
		posFrom = chessToCoord(str[-4:-2])
		piece = board[posFrom[0]][posFrom[1]]
		if mtype == "piececoord" and piece[1] != str[0]:
			return "There is a "+piecenames[piece]+" at "+str[-4:-2]+" and not a "+piecenames[str[0]]
	elif mtype[:8] == "castling":
		err = checkCastling(mtype[8:],player)
		return err
	else: # mtype = "doublespecial"
		listpiece1 = []
		listpiece2 = []
		for piece1 in pieceDict:
			if piece1[:2] == player+str[0] and pieceDict[piece1].status != 1:
				listpiece1.append(piece1)
			if piece1[:2] == `((not (int(player)-1))+1)`+str[1] and pieceDict[piece1].status != 1:
				listpiece2.append(piece1)
		count = 0
		for piece1 in listpiece1:
			for piece2 in listpiece2:
				if pieceDict[piece2].pos in pieceDict[piece1].moves():
					piece = piece1
					posTo = pieceDict[piece2].pos
					count = count + 1
		if count == 0:
			return "No "+piecenames[str[0]]+"s are able to take an opposition's "+piecenames[str[1]]
		elif count != 1:
			return "Be more specific"
	if mtype[:6] == "normal":
		if mtype == "normalpromotepawn":
			promoteTo = str[2]
			str = str[:-1]
		posTo = chessToCoord(str[-2:])
		count = 0
		for i in pieceDict:
			if i[:2] == pieceType and checkLegal(pieceDict[i],posTo) == 0 and pieceDict[i].status != 1:
				count = count + 1
				piece = i
		if count == 0:
			return "Piece can't move there"
		if count != 1:
			return "More than one piece can move there"
	if mtype[:7] == "special":
		posTo = chessToCoord(str[-2:])
		if mtype[7] == "x":
			axis = 0
			rankorfile = "-file"
			x = chessToCoord(str[-3]+"1")[0]
		else:
			axis = 1
			rankorfile = "-rank"
			x = chessToCoord("a"+str[1])[1]
		count = 0
		for i in pieceDict:
			if i[:2] == pieceType and pieceDict[i].status != 1 and pieceDict[i].pos[axis] == x and posTo in pieceDict[i].moves():
				count = count + 1
				piece = i
		if count == 0:
			return "There aren't any "+piecenames[pieceType[1]]+"s on the "+str[-3]+rankorfile
		elif count != 1:
			return "There is more than one "+piecenames[pieceType[1]]+" on the "+str[-3]+rankorfile+" which can move there. Be more specific"

	if checkLegal(pieceDict[piece],posTo) == 0:  # This is the main place where it connects with backend
		return carryout(piece, posTo, player, promoteTo, take)
	return "Move not Legal"

def printBoard(board):  # Prints board 
	i = True
	for j in reversed(range(len(board))):
		for k in range(len(board[j])):
			if i==True: 
				sys.stdout.write('\033[47m\033[30m '+board[k][j][:-1]+' \033[0m')
			else:
				sys.stdout.write(" "+board[k][j][:-1]+" ")
			i=not i
		print ""
		i=not i

def player(num):
	global fiftymoverule, quit, turn
	turn = int(not(turn-1))+1
	gameend = isGameEnd(num)
	if gameend != 0:
		quit = True
		print gameend
		return gameend
	print "Player",num,"turn: \n\n"
	while True:
		naturalInput = raw_input("Your move (standard chess notation):")
		result = run(num,naturalInput)
		if result == 0:
			fiftymoverule += 1
			break
		else:
			print result

turn = 1
#MAIN LOOP
quit = False
setupPieces(board)
while quit == False:
	printBoard(board)
	if turn == enpassant[1]:
		enpassant = [[0,0],0]
	player(str(turn))